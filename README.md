Small HOWTO install kicad on MacOS 10.14.6 Mojave

Some things could be optional and there could be more elegant ways to proceed

For more details

https://gitlab.com/kicad/packaging/kicad-mac-builder


Intall Xcode 11.3.1. and Command Line Tools 11.3.1.

https://developer.apple.com/download/more/


Download the kicad installer

	mkdir git
	cd git
	git clone https://gitlab.com/kicad/packaging/kicad-mac-builder.git

Install dyldstyle https://gitlab.com/adamwwolf/dyldstyle

	git clone https://gitlab.com/adamwwolf/dyldstyle.git
	cd dyldstyle
	python3 -m venv venv
	source venv/bin/activate
	pip install -e .

Quit terminal and relaunch it

Restore bash
	sudo dscl . -change /Users/root UserShell /bin/sh /bin/bash

Make symbolic link to wrangle-bundle ( is it a security problem ? )

	ln -s ~/git/dyldstyle/venv/bin/wrangle-bundle /usr/local/bin

Quit terminal and relaunch it

Make a copy of the directory /kicad-mac-builder and cd on the copy

Install the dependencies

	./ci/src/bootstrap.sh

Install opencascade

	brew install --build-from-source opencascade

Compile the dmg

	./build.py --target package-kicad-unified

If you have 2 cores 

	./build.py --jobs 2 --target package-kicad-unified

The dmg should be in /kicad-mac-builder/build/dmg

"Depending upon build configuration, what has already been downloaded, what has already been built, the computer and the network connection, this may take multiple hours and approximately 40G of disk space.

You can stop the build at any time by pressing Control-C."
